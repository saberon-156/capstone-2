// [SECTION] Dependencies and Modules
require('dotenv').config();  // Load environment variables from .env file
const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

// [SECTION] Server Setup
const app = express();
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));

// [SECTION] MongoDB URI and Port (Loaded from .env)
const mongoURI = process.env.MONGO_URL;  // MongoDB URI from .env file
const port = process.env.PORT || 5000;   // Port from .env file, default to 5000 if not provided

// [SECTION] Database Connect
mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connected to MongoDB Atlas Database.'))
  .catch((err) => console.log('Error connecting to MongoDB:', err));

// [SECTION] Server Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

// [SECTION] Server Responses
app.get('/', (req, res) => {
  res.send('E-commerce Back-End Project deployed successfully.');
});

// [SECTION] Start Server
app.listen(port, () => {
  console.log(`You are now live on port ${port}.`);
});
