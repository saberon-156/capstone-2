// [SECTION] Dependencies and Modules
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

// [SECTION] Environment Setup
dotenv.config();
const secret  = process.env.SECRET;

// [SECTION] Functionalities

// Create Access Token
module.exports.createAccessToken = (authUser) => {
    let userData = {
        id: authUser._id,
        email: authUser.email,
        isAdmin: authUser.isAdmin
    };

    // Add expiration time (e.g., 1 hour)
    return jwt.sign(userData, secret, { expiresIn: '1h' });
};

// Verify Token Middleware
module.exports.verify = (req, res, next) => {
    // Get token from Authorization header
    let token = req.headers.authorization;
    
    // Check if token exists
    if (typeof token !== 'undefined') {
        // Remove "Bearer " from the token
        token = token.slice(7, token.length);

        // Verify the token
        jwt.verify(token, secret, (err, payload) => {
            if (err) {
                // Send error if token is invalid or expired
                return res.status(401).json({ auth: 'Authorization Failed, Check Token.' });
            } else {
                // If valid, proceed to the next middleware or route
                req.user = payload;  // Optionally, attach the decoded payload to the request
                next();
            }
        });
    } else {
        // If token is missing
        return res.status(401).json({ auth: 'Authorization Failed, Check Token' });
    }
};

// Decode Token (Optional for extracting user info)
module.exports.decode = (accessToken) => {
    if (typeof accessToken !== 'undefined') {
        // Remove "Bearer " from the token
        accessToken = accessToken.slice(7, accessToken.length);

        // Decode token synchronously without verification
        try {
            const decoded = jwt.decode(accessToken, { complete: true });
            return decoded ? decoded.payload : null;
        } catch (err) {
            return null; // If decoding fails, return null
        }
    } else {
        return null;
    }
};
