// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const userBlueprint = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required.']
	},
	middleName: {
		type: String,
		required: [true, 'Middle name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is Required.']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number Required']
	},
	address: {
		type: String,
		required: [true, 'Address is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Order ID is Required."]
			 }
		}
	]
});

// [SECTION] Model
module.exports = mongoose.model("User", userBlueprint);