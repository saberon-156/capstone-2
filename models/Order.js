// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const orderBlueprint = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	productId: {
		type: String,
		required: [true, "Product ID is required"]
	},
	quantity: {
		type: Number,
		required: [true, "Quantity required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total amount is Required."]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

// [SECTION] Model
module.exports = mongoose.model("Order", orderBlueprint);