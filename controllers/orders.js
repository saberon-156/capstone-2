// [SECTION] Dependencies and Modules
const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');
const auth = require('../auth');


// [SECTION] Create Order
	module.exports.checkout = async (data) => {
		let prodId = data.productId;
		let qty = data.quantity;
		let userIdentity = data.userId

		const total = await Product.findById(prodId).then(result => result.price * qty);

		let userDb = await User.findById(userIdentity).then(result => {
			result.orders.push({productId: prodId});
			return result.save().then((saved, err) => {
				if (err) {
					return false
				} else {
					return true
				}
			})
		});

		let newOrder =  new Order({
			productId : prodId,
			quantity : qty,
			totalAmount : total,
			userId : userIdentity
		});

		return newOrder.save().then((ordered, err) => {
			if (ordered) {
				return `Order Successful! You may view your order below:
							${ordered}`
			} else {
				return 'An error has occured in storing your order.'
			}
		});
	};

// [SECTION] Get user order
	module.exports.getUserOrders = (data) => {
		return User.findById(data).then(user => {
			return user.firstName + ' ' + user.middleName + ' ' + user.lastName + ` 
			` + user.email + `
			Here are your transactions:
			` + user.orders;
		});
	};

// [SECTION] Get all user orders
	module.exports.getAllOrders = () => {
		return Order.find({}).then(result => {
			return result;
		});
	};
