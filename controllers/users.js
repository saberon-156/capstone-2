// [SECTION] Dependencies and Modules
const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// [SECTION] Functionalities [Create]

// Register new user
module.exports.registerUser = (reqBody) => {
  let { firstName, lastName, middleName, email, password, mobileNo, address } = reqBody;
  
  // Create new user
  const newUser = new User({
    firstName,
    lastName,
    middleName,
    email,
    password: bcrypt.hashSync(password, 10),  // Hash the password
    mobileNo,
    address
  });

  return User.findOne({ email }).then(found => {
    if (found) {
      return { success: false, message: 'Email already exists. Please use another email.' };
    } else {
      return newUser.save().then((user, err) => {
        if (user) {
          return { success: true, message: 'User registered successfully!', user };
        } else {
          return { success: false, message: 'Error registering user.' };
        }
      });
    }
  });
};

// Check if email exists
module.exports.checkEmailExistence = (reqBody) => {
  return User.find({ email: reqBody.email }).then(result => {
    if (result.length > 0) {
      return { success: false, message: 'Email already exists. Please use another email.' };
    } else {
      return { success: true, message: 'Email is available.' };
    }
  });
};

// Login user
module.exports.loginUser = (reqBody) => {
  let { email, password } = reqBody;

  return User.findOne({ email }).then(result => {
    if (!result) {
      return { success: false, message: 'Email not found.' };
    }

    // Compare password with the hashed one
    const isMatched = bcrypt.compareSync(password, result.password);
    if (isMatched) {
      const userData = result.toObject();
      const token = auth.createAccessToken(userData);  // Create JWT token
      return { success: true, accessToken: token };
    } else {
      return { success: false, message: 'Invalid password.' };
    }
  }).catch(err => {
    console.error('Login error:', err);
    return { success: false, message: 'Server error during login.' };
  });
};

// Purchase product
module.exports.purchase = async (data) => {
  let { userId, productId } = data;

  let isUserUpdated = await User.findById(userId).then(user => {
    user.orders.push({ productId });
    return user.save().then((saved, err) => {
      if (err) {
        return { success: false, message: 'Failed to update user orders.' };
      } else {
        return { success: true, message: 'User orders updated successfully.' };
      }
    });
  });

  let isProductUpdated = await Product.findById(productId).then(product => {
    product.orders.push({ userId });
    return product.save().then((saved, err) => {
      if (err) {
        return { success: false, message: 'Failed to update product orders.' };
      } else {
        return { success: true, message: 'Product orders updated successfully.' };
      }
    });
  });

  if (isUserUpdated.success && isProductUpdated.success) {
    return { success: true, message: 'Purchase successful.' };
  } else {
    return { success: false, message: 'Purchase failed. Contact support.' };
  }
};

// [SECTION] Functionalities [Retrieve]

// Get all users
module.exports.getAllUsers = () => {
  return User.find({}).then(result => {
    return { success: true, users: result };
  }).catch(err => {
    console.error('Error fetching users:', err);
    return { success: false, message: 'Server error fetching user list.' };
  });
};

// Get profile
module.exports.getProfile = (id) => {
  return User.findById(id).then(user => {
    if (user) {
      return { success: true, profile: user };
    } else {
      return { success: false, message: 'User not found.' };
    }
  }).catch(err => {
    console.error('Error fetching profile:', err);
    return { success: false, message: 'Server error fetching profile.' };
  });
};

// [SECTION] Functionalities [Update]

// Set User as Admin
module.exports.setAsAdmin = (userId) => {
  let updates = { isAdmin: true };

  return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
    if (admin) {
      return { success: true, message: 'User set as admin successfully.' };
    } else {
      return { success: false, message: 'Failed to set user as admin.' };
    }
  });
};

// Set User as Non-Admin
module.exports.setAsNonAdmin = (userId) => {
  let updates = { isAdmin: false };

  return User.findByIdAndUpdate(userId, updates).then((user, err) => {
    if (user) {
      return { success: true, message: 'User set as non-admin successfully.' };
    } else {
      return { success: false, message: 'Failed to set user as non-admin.' };
    }
  });
};

// Update Password
module.exports.changePassword = (reqBody) => {
  let { email, password, newpassword } = reqBody;

  return User.findOne({ email }).then(result => {
    if (!result) {
      return { success: false, message: 'Email does not exist.' };
    }

    const isMatched = bcrypt.compareSync(password, result.password);
    if (isMatched) {
      return User.findOneAndUpdate({ email }, { password: bcrypt.hashSync(newpassword, 10) }).then((savedUser, err) => {
        if (err) {
          return { success: false, message: 'Failed to update password.' };
        } else {
          return { success: true, message: 'Your password has been changed successfully.' };
        }
      });
    } else {
      return { success: false, message: 'Incorrect password.' };
    }
  });
};
