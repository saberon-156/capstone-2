// [SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controllers/users');
const auth = require('../auth');

// [SECTION] Routing Component
const route = exp.Router();

// [SECTION] User Routes (CREATE)

// Register new user
route.post('/register', async (req, res) => {
  try {
    const data = req.body;
    const outcome = await controller.registerUser(data);

    if (outcome === false) {
      return res.status(400).json({ success: false, message: 'Email already exists. Please use another email.' });
    }

    res.status(201).json({ success: true, message: 'User registered successfully!', user: outcome });
  } catch (error) {
    console.error('Registration error:', error);
    res.status(500).json({ success: false, message: 'Server error during registration' });
  }
});

// Login user
route.post('/login', async (req, res) => {
  try {
    const data = req.body;
    const outcome = await controller.loginUser(data);

    if (outcome.success) {
      return res.status(200).json({ success: true, accessToken: outcome.accessToken });
    } else {
      return res.status(400).json({ success: false, message: outcome.message });
    }
  } catch (error) {
    console.error('Login error:', error);
    res.status(500).json({ success: false, message: 'Server error during login' });
  }
});

// Check if email exists
route.post('/check-email', async (req, res) => {
  try {
    const message = await controller.checkEmailExistence(req.body);
    res.status(200).json({ success: true, message });
  } catch (error) {
    console.error('Error checking email:', error);
    res.status(500).json({ success: false, message: 'Server error checking email' });
  }
});

// [SECTION] User Routes (GET)

// Get all users (only accessible by admins)
route.get('/userlist', auth.verify, async (req, res) => {
  try {
    const { isAdmin } = req.user; // Use `req.user` from the verified token
    if (isAdmin) {
      const users = await controller.getAllUsers();
      res.status(200).json({ success: true, users });
    } else {
      res.status(403).json({ success: false, message: 'Unauthorized User. Admins only.' });
    }
  } catch (error) {
    console.error('Error fetching user list:', error);
    res.status(500).json({ success: false, message: 'Server error fetching user list' });
  }
});

// Get user profile
route.get('/profile', auth.verify, async (req, res) => {
  try {
    const userId = req.user.id; // Extract user ID from decoded token
    const profile = await controller.getProfile(userId);
    res.status(200).json({ success: true, profile });
  } catch (error) {
    console.error('Error fetching profile:', error);
    res.status(500).json({ success: false, message: 'Server error fetching profile' });
  }
});

// [SECTION] User Routes (UPDATE)

// Set User as Admin
route.put('/:userId/set-as-admin', auth.verify, async (req, res) => {
  try {
    const { isAdmin } = req.user; // Check if the logged-in user is an admin
    const id = req.params.userId;
    if (isAdmin) {
      const outcome = await controller.setAsAdmin(id);
      return res.status(outcome.success ? 200 : 400).json(outcome);
    } else {
      return res.status(403).json({ success: false, message: 'Unauthorized. Admins only.' });
    }
  } catch (error) {
    console.error('Error setting admin:', error);
    res.status(500).json({ success: false, message: 'Server error setting as admin' });
  }
});

// Set User as Non-Admin
route.put('/:userId/set-as-user', auth.verify, async (req, res) => {
  try {
    const { isAdmin } = req.user; // Check if the logged-in user is an admin
    const id = req.params.userId;
    if (isAdmin) {
      const outcome = await controller.setAsNonAdmin(id);
      return res.status(outcome.success ? 200 : 400).json(outcome);
    } else {
      return res.status(403).json({ success: false, message: 'Unauthorized. Admins only.' });
    }
  } catch (error) {
    console.error('Error setting as user:', error);
    res.status(500).json({ success: false, message: 'Server error setting as user' });
  }
});

// Change Password
route.put('/change-password', auth.verify, async (req, res) => {
  try {
    const userData = req.user; // Get user from the decoded token
    const data = req.body; // New password
    const outcome = await controller.changePassword(data);
    return res.status(outcome.success ? 200 : 400).json(outcome);
  } catch (error) {
    console.error('Error changing password:', error);
    res.status(500).json({ success: false, message: 'Server error changing password' });
  }
});

// [SECTION] Expose Route System
module.exports = route;
