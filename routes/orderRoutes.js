// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Create Order 
	route.post('/checkout', auth.verify, (req, res) => {
    	let token = req.headers.authorization; 
    	let payload = auth.decode(token);
    	let userId = payload.id	     
    	let isAdmin = payload.isAdmin  
    	let itemId = req.body.productId;
    	let qty = req.body.quantity
    	let data = {
    		userId: userId,
    		productId: itemId,
    		quantity: qty
    	};

    	(isAdmin) ? res.send("Admin privileges do not include purchasing a product.")
    	: controller.checkout(data).then(outcome => res.send(outcome));
    });

// [SECTION] Get user orders
	route.get('/myorders', auth.verify, (req, res) => {
		let payload = auth.decode(req.headers.authorization);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		isAdmin ? res.send('Admin privileges do not include purchasing a product.')
		: controller.getUserOrders(userId).then(outcome => {
			res.send(outcome);
		});
	});

// [SECTION] Get all orders and users
	route.get('/all', auth.verify , (req, res) => {
			let token = req.headers.authorization;
			let payload = auth.decode(token);
			let isAdmin = payload.isAdmin;
			isAdmin ? controller.getAllOrders().then(outcome => res.send(outcome))
			: res.send('Unauthorized User.')
		});

// [SECTION] Export Route System
	module.exports = route;
